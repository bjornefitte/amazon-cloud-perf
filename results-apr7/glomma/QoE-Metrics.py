#import scipy as sp
import pprint
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections

ls = []
lists_split = []
def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list[1:]

def split_ls (duration):
    global lists_split
    for l in ls:
      #print len(l)
      if duration == "second":
        chuncks = [l[x:x+30] for x in xrange(0, len(l), 30)]
        lists_split.append(chuncks) #30FPS
      elif duration == "minute":
        chuncks = [l[x:x+1800] for x in range(0, len(l), 1800)]
        lists_split.append (chuncks) #30FPS

def getmax():
    lk = []
    for l in ls:
       lk.append(max(l))
    return max(lk)
def outliers_per_duration (value, duration):

    for i,chuncks in enumerate (lists_split):
        results  = []
        #pprint.pprint(chuncks)
        #print len(chuncks)
        for j, chunk in enumerate (chuncks) :
            results.append(len ([x  for x in chunk if x > value]))

        ax = mpl.figure()
        fig = ax.add_subplot(111)
        fig.set_title('number of values over %s ms in case of %s noise' % (str(value), str(i+1)))
        fig.set_xlabel('time unit: '+duration)
        fig.set_ylabel('Number of values over '+ str(value) +" ms")
        #fig.set_ylim([0,500])
        fig.plot( [x for x in range(0,len(results))], results,'ro', label= str(i+1)+" noise generators")
      #  for xc in range(0,len(results)):
       #     fig.axvline (x=xc)
        #fig.close()
       # mpl.show()
        mpl.savefig("images/%s_nm_over_%s_per_%s.png"%(str(i+1), str(value), duration))

def ieth_percentile (percentile):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('number of values in 99th percentile for different numbers of noise makers')
    fig.set_xlabel('number of noise makers')
    fig.set_ylabel('Number of values in the 99th percentile')
    fig.set_ylim([0,100])
    fig.plot( [x for x in range(0,len(ls))], [np.percentile(l, percentile) for l in ls ],'ro')
    #for xc in range(0,len(ls)):
     #       fig.axvline (x=xc)
    #mpl.show()
    mpl.savefig("images/percentile_%s.png"%percentile)
if __name__ == "__main__":
    head,_ = pth.split(sys.argv[1])
    for element in sys.argv[1:]:
         l= open_and_parse(element)
         ls.append(l)

    split_ls("minute")
    outliers_per_duration(100.0/3.0,"minute")
    outliers_per_duration (200.0/3.0, "minute")

    lists_split = []
    split_ls("second")
    outliers_per_duration (100.0/3.0, "second")
    outliers_per_duration (200/3.0, "second")
    ieth_percentile(99)
