import sys
import re
import pprint
import time
import matplotlib.pyplot as plt
import matplotlib.dates as mdt
import numpy as np
from matplotlib.font_manager import FontProperties
from datetime import datetime
inputf = open(sys.argv[1],"r")
list = inputf.readlines()
count = 0
time_cpu_mem = {}
ttime = None
idle_cpu = 0
total_mem = 0
used_mem = 0
process_cpu = 0
process_mem = 0
res = []
kappa = sys.argv[1].split('.')[0]
gamma = sys.argv[1].split('.')[1]
start = ""
if gamma == "dc":
    gamma = "DeepContext"
else :
    gamma = "Unmodified OvS"

def get_sec(time_str):
    h, m, s = time_str.split(':')
    hs, ms, ss = start.split(':')
    return (int(h)-int(hs))*3600 + (int(m)-int(ms)) * 60 + (int(s)-int(ss))

print kappa
print "time", "idle", "total_mem", "used_mem" , "process_cpu" , "process_mem"
for line in list :
    split= re.split(" +",line)
       
    count+=1
   # print count
    if count % 9 == 1:
        ttime =  split[2]#datetime.strptime(split [2], "%H:%M:%S") #split [2]
        if count == 1:
            start = split[2]
    #if count % 9 == 3:
        #idle_cpu = float(split[7])
    if count %9 == 4 :
        pass
        #total_mem = float(split[2])
        #used_mem = float(split [4])
    if count % 9 == 8:
        # print split
         process_cpu = float(split[9])
         process_mem = float(split[10])

    if count % 9 == 0 :
        print ttime, idle_cpu, total_mem, used_mem, process_cpu, process_mem
        res.append((ttime, idle_cpu, used_mem/total_mem*100, process_cpu, process_mem))
        ttime = None
        idle_cpu = 0
        total_mem = 0
        used_mem = 0
        process_cpu = 0
        process_mem = 0

dates = [x[0] for x in res]
x  = [get_sec(y) for y in dates]
idle_cpu_list = [y[1] for y in res]
used_mem_ratio = [y[2] for y in res]
process_cpu_ratio = [y[3] for y in res]
process_mem_usage = [y[4] for y in res]
#r = plt.plot_date(x,idle_cpu_list, label="idle cpu ratio (machine)")
#plt.setp(r,color = "red")
#b = plt.plot_date(x,used_mem_ratio,label = "used memory ratio (machine)")
#plt.setp(b,color = "blue")
g = plt.plot(x,process_cpu_ratio,label = "CPU ratio used by "+kappa)
plt.setp(g,color = "blue")
#bl = plt.plot(x,process_mem_usage,label = "Memory ratio used by "+kappa)
#plt.setp(bl,color = "red")
#plt.gca().xaxis.set_major_formatter(mdt.DateFormatter("%H:%M:%S"))
#plt.gcf().autofmt_xdate()
fontP = FontProperties()
fontP.set_size('small')
plt.legend(bbox_to_anchor=(0., 1.00, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)
plt.title('Resource usage by '+ kappa +" in " + gamma, y = -0.10 )
plt.ylabel('Resource Ratio (%)')
#plt.ylim((0,max(max(process_cpu_ratio),max(process_mem_usage))))
plt.ylim ((0,50))
plt.xlim((0,max(x)))
plt.xlabel('time (s)', x = 1)
plt.grid()
#plt.show()

    

print "MEAN CPU    : "+str (np.mean (np.array(process_cpu_ratio)))
print "MEDIAN CPU  : " + str (np.median (np.array(process_cpu_ratio)))
print "STD DEV CPU : " + str (np.std(np.array(process_cpu_ratio)))
print "MAX CPU     : " + str(max(process_cpu_ratio))
print "MEAN RAM:   : "+str (np.mean (np.array(process_mem_usage)))
print "MEDIAN RAM  : "  + str (np.median (np.array(process_mem_usage)))
print "STD DEV RAM : " + str (np.std(np.array(process_mem_usage)))
print "MAX RAM     : " + str( max(process_mem_usage))
