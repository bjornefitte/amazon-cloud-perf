#import scipy as sp
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
total = 0
dict_times = {}
dict_medians = {}
dict_avg = {}
dict_stddev = {}
dict_absdev = {}
dict_minmax = {}
dict_iqr = {}
dict_90_10 = {}
def iqr (list, lower, upper):
    q_up, q_low = np.percentile(list, [upper ,lower])
    return q_up - q_low

def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list

def calculate_stats(key, value_list):
    dict_medians[key] = np.median(value_list)
    dict_stddev[key] = np.std(value_list)
    dict_avg[key] = np.average(value_list)
    dict_minmax[key] = (np.min(value_list), np.max(value_list))
    dict_absdev[key] = robust.mad(value_list)
    dict_iqr[key] = iqr(value_list, 25, 75);
    dict_90_10 [key] =iqr (value_list, 90, 10)

def plot_medians (head, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('median delta times for different saturations \n in %s for a duration of 5 minutes'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('median time difference (ms)')
    fig.plot([x/total * 100 for x in dict_medians.keys()],dict_medians.values(), 'ro')
    #mpl.show()
    mpl.savefig('images/%s/%s_medians.png'%(head,machine))
def plot_stddev (head, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('std dev delta times for different saturations \n in %s for a duration of 5 minutes'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('stddev time difference (ms)')
    fig.plot([x/total * 100 for x in dict_stddev.keys()],dict_stddev.values(), 'ro')
    #mpl.show()
    mpl.savefig('images/%s/%s_stddev.png'%(head,machine))
def plot_absdev (head, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('abs dev times for different saturations \n in %s for a duration of 5 minutes'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('absdev time difference (ms)')
    fig.plot([x/total * 100 for x in dict_absdev.keys()],dict_absdev.values(), 'ro')
    #mpl.show()
    mpl.savefig('images/%s/%s_absdev.png'%(head,machine))
def plot_iqr (head, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('75-25 IQR delta times for different saturations \n in %s for a duration of 5 minutes'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('IQR time difference (ms)')
    fig.plot([x/total * 100 for x in dict_iqr.keys()],dict_iqr.values(), 'ro')
    #mpl.show()
    mpl.savefig('images/%s/%s_75_25_iqr.png'%(head,machine))
def plot_90_10 (head, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('90-10 IQR delta times for different saturations \n in %s for a duration of 5 minutes'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('IQR time difference (ms)')
    fig.plot([x/total * 100 for x in dict_90_10.keys()],dict_90_10.values(), 'ro')
    #mpl.show()
    mpl.savefig('images/%s/%s_90_10_IQR.png'%(head,machine))
def plot_minmax (head, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('delta times for different saturations \n in %s for a duration of 5 minutes'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('time difference (ms)')
    fig.plot([x/total * 100 for x in dict_minmax.keys()],[float(x) for (x,y) in dict_minmax.values()], 'ro', color="red")
    fig.plot([x/total * 100 for x in dict_minmax.keys()],[float(y) for (x,y) in dict_minmax.values()], 'ro', color="blue")
    #mpl.show()
    mpl.savefig('images/%s/%s_min_max.png'%(head,machine))
if __name__ == "__main__":
    total = float(sys.argv[1])
    machine = sys.argv[2].split('/')[-2]
    head,_ = pth.split(sys.argv[2])
    for element in sys.argv[2:]:
       l = open_and_parse(element)
       objects = int(element.split('/')[-1].split('.')[0].split('_')[-1])
       dict_times[objects] = l

    for key, value in dict_times.iteritems():
        calculate_stats(key, value)
    plot_medians(head, machine)
    plot_absdev(head, machine)
    plot_stddev(head, machine)
    plot_iqr(head,machine)
    plot_90_10(head,machine)
    plot_minmax(head,machine)
