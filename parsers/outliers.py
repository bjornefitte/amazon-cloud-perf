#import scipy as sp
import pprint
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections
import time
import itertools
m3_me_list = []
t2_sm_list = []
t2_mi_list = []
glomma_list = []

outliers_m3_me = []
outliers_t2_sm = []
outliers_t2_mi = []
outliers_glomma = []
def plot (unit):
      ax = mpl.figure()
      fig = ax.add_subplot(111)
      fig.set_xlabel('trials')
      fig.set_ylabel('average outliers per %s' % (unit))
      fig.plot([x+1 for x in range(0,len(outliers_m3_me))],outliers_m3_me,'-ro',  label="m3.medium", color="red")
      fig.plot([x+1 for x in range(0,len(outliers_t2_sm))],outliers_t2_sm,'-rx',  label="t2.small", color ="blue")
      fig.plot([x+1 for x in range(0,len(outliers_t2_mi))],outliers_t2_mi,'-rv',  label="t2.micro", color="green")
      fig.plot([x+1 for x in range(0,len(outliers_glomma))],outliers_glomma,'-r*',  label="local VM", color="cyan")
      mpl.legend(loc='best')
      mpl.show()

def find_outliers (threshold,duration):
    for l in m3_me_list:
        temporary = [x for x in l if x>threshold]
        outliers_m3_me.append(float (len(temporary))/duration)

    for l in t2_sm_list:
        temporary = [x for x in l if x>threshold]
        outliers_t2_sm.append (float (len(temporary))/duration)

    for l in t2_mi_list:
        temporary = [x for x in l if x>threshold]
        outliers_t2_mi.append (float (len(temporary))/duration)

    for l in  glomma_list:
        temporary = [x for x in l if x> threshold]
        outliers_glomma.append(float(len(temporary))/duration)

def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list[1:]


if __name__ == "__main__":
    for element in sys.argv[3:]:
         head , _ = pth.split(element)
         head = head.split("/")[-1]
         l = open_and_parse(element)
         if head == "m3-medium-results":
             m3_me_list.append(l)
         elif head == "t2-micro-results":
             t2_sm_list.append(l)
         elif head == "t2-small-results":
             t2_mi_list.append(l)
         elif head == "glomma-vm":
             glomma_list.append(l)

    find_outliers(float(sys.argv[1]), float(sys.argv[2]))
    plot("minute")