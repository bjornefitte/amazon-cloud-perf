#import scipy as sp
import pprint
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections
import time
import itertools
m3_me_list = []
t2_sm_list = []
t2_mi_list = []
glomma_list = []






def plot ():
     ax = mpl.figure()
     fig = ax.add_subplot(111)
     fig.set_xlabel('trials')
     fig.set_ylabel('balls')
     fig.plot([x+1 for x in range(0,len(m3_me_list))],m3_me_list,'-ro',  label="m3.medium", color="red")
     fig.plot([x+1 for x in range(0,len(t2_sm_list))],t2_sm_list,'-rx',  label="t2.small", color ="blue")
     fig.plot([x+1 for x in range(0,len(t2_mi_list))],t2_mi_list,'-rv',  label="t2.micro", color="green")
     fig.plot([x+1 for x in range(0,len(glomma_list))],glomma_list,'-r*',  label="local VM", color="cyan")
     mpl.legend(loc='best')
     mpl.show()

if __name__ == "__main__":
    for element in sys.argv[1:]:
         head , _ = pth.split(element)
         head = head.split("/")[-1]
         elenbr  = element.split("_")[-1].split(".")[0]
         if head == "m3-medium-results":
             m3_me_list.append(int(elenbr))
         elif head == "t2-micro-results":
             t2_sm_list.append(int (elenbr))
         elif head == "t2-small-results":
             t2_mi_list.append(int(elenbr))
         elif head == "glomma-vm":
             glomma_list.append(int(elenbr))

    plot()