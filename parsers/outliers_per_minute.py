#import scipy as sp
import pprint
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections
import time
ls = []

lists_split = []
to_plot = {}
def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list[1:]

def outliers_per_duration (value, duration):
    outliers_per_mn = []
    for i,l in enumerate(ls):
        outliers = [x for x in l if x> value]
        outliers_per_mn.append (float ( len(outliers))/duration )


    to_plot [str(value)] = outliers_per_mn
colour=['red', 'orange', 'blue', 'green', 'brown']
def plot (unit):
      ax = mpl.figure()
      fig = ax.add_subplot(111)
      fig.set_title('number of outliers per %s for varying number of background noise makers' % (unit))
      fig.set_xlabel('trial #')
      fig.set_ylabel('number of outliers per %s' % (unit))
      for key, value in to_plot.iteritems():
            print key, value
            fig.plot( [x for x in range(0,len(ls))], value, label= "outliers per %s with threshold being %s"%(str (unit), str(key)),color=colour[int(float(key)*3.0/50)])
      fig.set_ylim([0,1000])
      mpl.legend(loc='best')
      mpl.show()

if __name__ == "__main__":
    head,_ = pth.split(sys.argv[1])
    for element in sys.argv[1:]:
         l= open_and_parse(element)
         ls.append(l)

    outliers_per_duration (100.0/3, 5.0)
    outliers_per_duration (150.0/3, 5.0)
    outliers_per_duration (200.0/3, 5.0)

    plot("minute")