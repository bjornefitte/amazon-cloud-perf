#import scipy as sp
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections

ls = []
def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list[1:]

def plot_boxplot(head):
    ax = mpl.figure(figsize=(20,10))
    fig = ax.add_subplot(111)
    fig.set_title('Boxplot for different saturations on %s'%(head))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('time difference(ms)')
    fig.boxplot (ls)
    fig.set_xticklabels([25, 50, 75, 100])
    fig.set_ylim([0,100])
    #mpl.show()
    mpl.savefig('%s/boxplot.png'%(head))

def plot_minmax (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('delta_times vs frame number in case of %s noise' % head)
    fig.set_xlabel('frame nbr')
    fig.set_ylabel('time difference (ms)')
    for i in range(0, len(ls)):
        label = str ( int ( (i+1) * 450/4))
        print label
        fig.plot([x for x in range(0,len(ls[i]))],ls[i], label=label)
        fig.set_ylim([25,60])
    mpl.legend(loc='best')
   # mpl.show()

    mpl.savefig('%s/time_series.png'%(head))

if __name__ == "__main__":
    head,_ = pth.split(sys.argv[1])
    for element in sys.argv[1:]:
         l= open_and_parse(element)
         ls.append(l)

    plot_minmax(head)
    plot_boxplot(head)
