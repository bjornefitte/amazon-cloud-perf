#import scipy as sp
import pprint
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections
import time
import itertools
ls = []
biglist =[]
lists_split = []
to_plot = {}
def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list[1:]
def plot():
     biglist = list(itertools.chain.from_iterable(ls))
     #print biglist
     ax = mpl.figure()
     fig = ax.add_subplot(111)
     fig.set_xlabel('frame nbr')
     fig.set_ylabel('time difference (ms)')
     label = "30 runs on a t2.small instance"
     fig.set_title('delta_times vs frame number for 30 runs on a t2.small instance')
     fig.plot([x for x in range(0,len(biglist))],biglist, label=label)
     fig.set_ylim([20,300])
     mpl.legend(loc='best')
     mpl.savefig("t2.small-plot.png")

def plot_y(ys):
     ax = mpl.figure()
     fig = ax.add_subplot(111)
     fig.set_xlabel('trial #')
     fig.set_ylabel('nbr of elements generated')
     label = "30 runs on a t2.small instance"
     fig.set_title('Changes in the number of elements for 30 runs on t2.small instance')
     fig.plot([x for x in range(0,len(ys))],ys,'ro',  label=label)
     #fig.set_ylim([190,290])
     mpl.legend(loc='best')
     mpl.savefig("t2.small-elements.png")

if __name__ == "__main__":
    head,_ = pth.split(sys.argv[1])
    ys = []
    for element in sys.argv[1:]:
         elenbr  = element.split("_")[-1].split(".")[0]
         ys.append(elenbr)
         l= open_and_parse(element)
         ls.append(l)
    plot()
    plot_y(ys)