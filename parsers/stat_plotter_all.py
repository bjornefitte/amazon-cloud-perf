#import scipy as sp
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections

total_vm = float(450)
total_docker = float(450)
total_native = float(450)
dict_times_vm = collections.OrderedDict()
dict_medians_vm = collections.OrderedDict()
dict_avg_vm = collections.OrderedDict()
dict_stddev_vm = collections.OrderedDict()
dict_absdev_vm = collections.OrderedDict()
dict_minmax_vm = collections.OrderedDict()
dict_iqr_vm = collections.OrderedDict()
dict_90_10_vm = collections.OrderedDict()

dict_times_native = collections.OrderedDict()
dict_medians_native = collections.OrderedDict()
dict_avg_native = collections.OrderedDict()
dict_stddev_native = collections.OrderedDict()
dict_absdev_native = collections.OrderedDict()
dict_minmax_native = collections.OrderedDict()
dict_iqr_native = collections.OrderedDict()
dict_90_10_native = collections.OrderedDict()


dict_times_docker = collections.OrderedDict()
dict_medians_docker = collections.OrderedDict()
dict_avg_docker = collections.OrderedDict()
dict_stddev_docker = collections.OrderedDict()
dict_absdev_docker = collections.OrderedDict()
dict_minmax_docker = collections.OrderedDict()
dict_iqr_docker = collections.OrderedDict()
dict_90_10_docker = collections.OrderedDict()

N = 3
ind = np.arange(N)  # the x locations for the groups
width = 0.25

def iqr (list, lower, upper):
    q_up = np.percentile(list,upper)
    q_low = np.percentile(list, lower)
    return q_up - q_low

def open_and_parse(file):
    f = open(file,"r")
    i = 0
    unparsed_list = []
    for line in f:
        if i != 0:
            unparsed_list.append(float (line.strip().split(' ')[-1])/1e3)
        i+=1
    f.close()
    return unparsed_list

def calculate_stats(key, value_list, dict_medians, dict_stddev, dict_avg, dict_minmax, dict_absdev, dict_iqr, dict_90_10):
    dict_medians[key] = np.median(value_list)
    dict_stddev[key] = np.std(value_list)
    dict_avg[key] = np.average(value_list)
    dict_minmax[key] = (np.min(value_list), np.max(value_list))
    dict_absdev[key] = robust.mad(value_list)
    dict_iqr[key] = iqr(value_list, 25, 75);
    dict_90_10 [key] =iqr (value_list, 90, 10)

def plot_medians (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('median delta times for different saturations \n for a duration of 5 minutes')
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('median time difference (ms)')
   # n_plot, = fig.plot([x/total_native * 100 for x in dict_medians_native.keys()],dict_medians_native.values(), color="blue", label="Native")
   # v_plot, = fig.plot([x/total_vm * 100 for x in dict_medians_vm.keys()],dict_medians_vm.values(),  color="red", label = "VM")
   # d_plot, = fig.plot([x/total_docker * 100 for x in dict_medians_docker.keys()],dict_medians_docker.values(),  color="green" ,label ="docker")
    fig.bar(ind-width, dict_medians_native.values(), width, color='red', label="native")
    fig.bar(ind, dict_medians_vm.values(),width, color='green', label ="vm")
    fig.bar(ind+width, dict_medians_docker.values(), width, color='blue', label ="docker")
    fig.set_xticks(ind )
    fig.set_xticklabels([x/total_native * 100 for x in dict_medians_native.keys()] )
    mpl.legend(loc='best')

    #mpl.show()
    mpl.savefig('images/%s_medians.png'%(head))

def plot_stddev (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('std dev delta times for different saturations for a duration of 5 minutes')
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('stddev time difference (ms)')
   # n_plot, = fig.plot([x/total_native * 100 for x in dict_stddev_native.keys()],dict_stddev_native.values(), 'ro', color="blue", label="native")
   # v_plot, = fig.plot([x/total_vm * 100 for x in dict_stddev_vm.keys()],dict_stddev_vm.values(), 'ro', color="red", label="vm")
   # d_plot, = fig.plot([x/total_docker * 100 for x in dict_stddev_docker.keys()],dict_stddev_docker.values(), 'ro', color="green", label="docker")
    fig.bar(ind-width, dict_stddev_native.values(), width, color='red', label="native")
    fig.bar(ind, dict_stddev_vm.values(),width, color='green', label ="vm")
    fig.bar(ind+width, dict_stddev_docker.values(), width, color='blue', label ="docker")
    fig.set_xticks(ind )
    fig.set_xticklabels([x/total_native * 100 for x in dict_stddev_native.keys()] )
    #mpl.show()
    mpl.legend(loc='best')
    mpl.savefig('images/%s_stddev.png'%(head))

def plot_absdev (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('abs dev times for different saturations \n  for a duration of 5 minutes')
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('absdev time difference (ms)')
   # n_plot, = fig.plot([x/total_native * 100 for x in dict_absdev_native.keys()],dict_absdev_native.values(), 'ro', color="blue", label = "native")
   # v_plot, = fig.plot([x/total_vm * 100 for x in dict_absdev_vm.keys()],dict_absdev_vm.values(), 'ro', color="red", label = "vm")
   # d_plot, = fig.plot([x/total_docker * 100 for x in dict_absdev_docker.keys()],dict_absdev_docker.values(), 'ro', color="green", label ="docker")
    #mpl.show()
    fig.bar(ind-width, dict_absdev_native.values(), width, color='red', label="native")
    fig.bar(ind, dict_absdev_vm.values(),width, color='green', label ="vm")
    fig.bar(ind+width, dict_absdev_docker.values(), width, color='blue', label ="docker")
    fig.set_xticks(ind )
    fig.set_xticklabels([x/total_native * 100 for x in dict_absdev_native.keys()] )
    mpl.legend(loc='best')
    mpl.savefig('images/%s_absdev.png'%(head))

def plot_iqr (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('75-25 IQR delta times for different saturations \n  for a duration of 5 minutes')
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('IQR time difference (ms)')
    #n_plot, = fig.plot([x/total_native * 100 for x in dict_iqr_native.keys()],dict_iqr_native.values(), 'ro', color="blue", label = "native")
    #v_plot, = fig.plot([x/total_vm * 100 for x in dict_iqr_vm.keys()],dict_iqr_vm.values(), 'ro', color="red", label = "vm")
    #d_plot, = fig.plot([x/total_docker * 100 for x in dict_iqr_docker.keys()],dict_iqr_docker.values(), 'ro', color="green", label="docker")
    #mpl.show()
    fig.bar(ind-width, dict_iqr_native.values(), width, color='red', label="native")
    fig.bar(ind, dict_iqr_vm.values(),width, color='green', label ="vm")
    fig.bar(ind+width, dict_iqr_docker.values(), width, color='blue', label ="docker")
    fig.set_xticks(ind )
    fig.set_xticklabels([x/total_native * 100 for x in dict_iqr_native.keys()] )
    mpl.legend(loc='best')
    mpl.savefig('images/%s_75_25_iqr.png'%(head))

def plot_90_10 (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('90-10 IQR delta times for different saturations \n for a duration of 5 minutes')
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('IQR time difference (ms)')
    #n_plot, = fig.plot([x/total_native * 100 for x in dict_90_10_native.keys()],dict_90_10_native.values(), 'ro', color="blue", label = "native")
    #v_plot, = fig.plot([x/total_vm * 100 for x in dict_90_10_vm.keys()],dict_90_10_vm.values(), 'ro', color="red", label = "vm")
    #d_plot, =fig.plot([x/total_docker * 100 for x in dict_90_10_docker.keys()],dict_90_10_docker.values(), 'ro', color="green", label="docker")
    #mpl.show()
    fig.bar(ind-width, dict_90_10_native.values(), width, color='red', label="native")
    fig.bar(ind, dict_90_10_vm.values(),width, color='green', label ="vm")
    fig.bar(ind+width, dict_90_10_docker.values(), width, color='blue', label ="docker")
    fig.set_xticks(ind )
    fig.set_xticklabels([x/total_native * 100 for x in dict_90_10_native.keys()] )
    mpl.legend(loc='best')
    mpl.savefig('images/%s_90_10_IQR.png'%(head))

def plot_minmax (head):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('delta times for different saturations \n  for a duration of 5 minutes')
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('time difference (ms)')
    fig.plot([x/total_native * 100 for x in dict_minmax_native.keys()],[float(y) for (x,y) in dict_minmax_native.values()], 'ro', color="blue", label="native")
    fig.plot([x/total_native * 100 for x in dict_minmax_native.keys()],[float(y) for (x,y) in dict_minmax_native.values()], 'ro', color="blue")
    fig.plot([x/total_vm * 100 for x in dict_minmax_vm.keys()],[float(y) for (x,y) in dict_minmax_vm.values()], 'ro', color="red", label = "vm")
    fig.plot([x/total_vm * 100 for x in dict_minmax_vm.keys()],[float(y) for (x,y) in dict_minmax_vm.values()], 'ro', color="red")
    fig.plot([x/total_docker * 100 for x in dict_minmax_docker.keys()],[float(y) for (x,y) in dict_minmax_docker.values()], 'ro', color="green", label ="Docker")
    fig.plot([x/total_docker * 100 for x in dict_minmax_docker.keys()],[float(y) for (x,y) in dict_minmax_docker.values()], 'ro', color="green")
    #mpl.show()
    mpl.legend(loc='best')
    mpl.savefig('images/%s_max.png'%(head))

def plot_boxplot(head, machine, datum, total):
    data = collections.OrderedDict(sorted(datum.items()))
    ax = mpl.figure(figsize=(20,10))
    fig = ax.add_subplot(111)
    fig.set_title('Boxplot for different saturations on %s'%(machine))
    fig.set_xlabel('saturation (%)')
    fig.set_ylabel('time difference(ms)')
    fig.boxplot (data.values())
    fig.set_xticklabels([round(x/total*100 )for x in data.keys()])
    fig.set_ylim([0,100])
    #mpl.show()
    mpl.savefig('images/%s_boxplot.png'%(head))
    return

def plot_cdf (time, machine):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    colours = {225 : 'red', 450 :  'green', 900 : 'blue'}
    fig.set_title('CDF for different saturations on %s'%(machine))
    fig.set_xlabel('Time difference (ms)')
    fig.set_ylabel('CDF (%)')
    for key,val in time.iteritems():
        value = np.sort(val)
        yvals=np.arange(len(value))/float(len(value)-1)
        line = mpl.plot(value,yvals, label=key, color = colours[key])
    mpl.legend(loc='best')
   # mpl.show()
    mpl.savefig('images/%s_cdf.png'%(machine))
    return

if __name__ == "__main__":
    info = "overall"
    for element in sys.argv[1:]:
       head,_ = pth.split(element)
       l = open_and_parse(element)
       objects = int(element.split('/')[-1].split('.')[0].split('_')[-1])
       if head == "native":
         dict_times_native[objects] = l
       elif head == "vm":
         dict_times_vm[objects] = l
       elif head == "docker" :
           dict_times_docker[objects] = l

    for key, value in dict_times_native.iteritems():
        calculate_stats(key, value, dict_medians_native, dict_stddev_native, dict_avg_native,
                        dict_minmax_native, dict_absdev_native, dict_iqr_native, dict_90_10_native)
    for key, value in dict_times_vm.iteritems():
        calculate_stats(key, value, dict_medians_vm, dict_stddev_vm,
                        dict_avg_vm,dict_minmax_vm, dict_absdev_vm, dict_iqr_vm, dict_90_10_vm)
    for key, value in dict_times_docker.iteritems():
        calculate_stats(key, value, dict_medians_docker, dict_stddev_docker, dict_avg_docker,
                        dict_minmax_docker, dict_absdev_docker, dict_iqr_docker, dict_90_10_docker)

    plot_medians(info)
    plot_stddev(info)
    plot_absdev(info)
    plot_iqr(info)
    plot_90_10(info)
    plot_minmax(info)
    plot_boxplot("native","native", dict_times_native, total_native)
    plot_boxplot("vm","vm", dict_times_vm, total_vm)
    plot_boxplot("docker","docker", dict_times_docker, total_docker)
    plot_cdf(dict_times_native, "native")
    plot_cdf(dict_times_docker, "docker")
    plot_cdf(dict_times_vm, "vm")