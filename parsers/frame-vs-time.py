import ply.lex as lex
import os.path as pth
import pprint
import numpy as np
from statsmodels import robust
import os.path as pth
import matplotlib.pyplot as mpl
import sys
import collections
import time
ls = []
to_plot ={}
tokens = ['TIME', 'INT']

t_TIME = r'^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$'
t_INT = r'[0-9]+'
t_ignore  = ' \t.'
def t_error(t):
        #print("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

lexer = lex.lex()
def parse (input_string):
    ret = []
    lexer.input (input_string)
    while True:
     tok = lexer.token()
     if not tok:
        break      # No more input
     ret.append((tok.type, tok.value))
    return ret

def plot():
     #print biglist
     ax = mpl.figure()
     fig = ax.add_subplot(111)
     fig.set_xlabel('time (s)')
     fig.set_ylabel('interframe time (ms)')
     fig.plot ([x for (x,y) in ls ],[y for (x,y) in ls])
     fig.set_ylim([20,70])
     mpl.legend(loc='best')
     mpl.show()

def open_and_parse(file):
    f = open(file,"r")
    time_start = 0
    unparsed_list = []
    for i,line in enumerate (f):
        l = parse (line.strip())
        if i == 1:
            time_start =  float (long (l[-2][1])/1e9) + long (l[-3][1])
        to_insert =  float (long (l[-2][1])/1e9) + long (l[-3][1]) - time_start, int (l[-1][1]) / 1e3

        unparsed_list.append(to_insert)
    return unparsed_list

if __name__ == "__main__":
    ls = open_and_parse(sys.argv[1])
    print ls
    plot()