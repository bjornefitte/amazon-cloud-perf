//
// Bencher.h
//

#include <stdio.h>
#include <time.h>
#include <vector>
#include "Object.h"
#include "Clock.h"
#include <sstream>
#define NANOCONST 1e9
#define SEC 60
#define LAST 30
#define ITERATION_MAX 3
#define TARGET_LIMIT  1.1 * df::FRAME_TIME_DEFAULT
typedef struct {
    long c;
    struct timespec ts;
} nanosec;

class Bencher : public df::Object {
private:
    int step_count;        // number of steps
    // int q[LAST];			// queue of game loop times
    //int head;			// head of queue
    //int iteration;		// iteration number
    df::Clock clock;        // keep track of loop timing
    std::vector<long int> frames_delta; // Vector of Deltas !
    std::vector<struct timespec> frames_timespec;
    std::string log ;
    double start_time;
    long duration;
    int ballnbrs;
    struct timespec tmp;
    nanosec ns;
    //int bench_mark;              // number of objects supported
    //bool do_particles;		// true if particle benchmark
    //void particleBurst();		// add particle burst

public:
    Bencher(char *time, char *number);

    int getBenchMark();

    void stopBounce();

    int eventHandler(const df::Event *p_e);
};


