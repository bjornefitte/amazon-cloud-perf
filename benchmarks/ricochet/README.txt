Bounce - a Dragonfly Benchmark
Copyright: Mark Claypool and WPI, 2014, 2015

v3.0

The Bounce "game" continually creates game objects (bouncing balls)
until the engine can no longer keep up.  The balls move with random
speeds and directions and collide with other balls.  The balls are
created at a rate of 1 ball per frame (so, about 30 balls per second).
Bounce runs three games and then produces a final report of the
average number of game objects Dragonfly can support under the tested
conditions.

HOW TO USE

LINUX:

0) Extract the benchmark and edit the Makefile.

1) Compile by typing:

  make depend

then:

  make clean

then:
  
  make

2) Run by typing:
  
  bounce

3) When done, the benchmark reports the maximum number of objects on
the screen.

WINDOWS (Visual Studio 2013):

0) Extract the benchmark and open the Visual Studio solution in
directory vs-2013/.

1) Compile (hotkey F7).

2) Run (hotkey F5).
  
3) When done, dragonfly.log reports the maximum number of objects.


DETAILS

+ Initially there are no Balls.

+ One Ball is created per step (so, about 30/second).

+ Balls have random speed (from .1 to 1 spaces/step) and direction.

+ Balls are solid and collide with other objects and the view boundary.

+ The frame time is recorded for the latest 30 steps (so, about 1 second).

+ If the median frame time is 10% over the target frame time (33 ms)
  then the iteration is done.

+ When an iteration is complete, the number of Balls is recorded.

+ After three iterations are run, the number of Balls over the three
  runs is averaged to provide an estimate of the maximum number of
  objects the setup can support.


USES

Overall, the number of objects (B) returned by Bounce provides a
“reasonable” maximum number of moving objects Dragonfly can support
under the tested environment (this means for that specfic computer,
operating system, and display device).

If there is just a single moving object, Dragonfly can support about B
times as many objects, so B*B (e.g. a game with a moving Hero and lots
of Walls).

Note, however, that any game-code computations will reduce the number
of objects that can be supported since bounce has minimal game code.

In general:

   B = estimated maximum reported by Bounce
   M = number of moving objects
   S = number of static (non-moving) objects

For "good" game performance, ensure:

  M * (M + S) <= (B*B)


NOTES

+ The Bounce benchmark uses a sliding window to keep track of
performance.  This provides a tradeoff between an instantaneous value
(using just the most recent frame time) and a continuous value (using
all the frame times from the start).

+ For the frame time statistic, a median is used to avoid spurious
outliers that may not reflect the true, sustained load the setup can
handle.

+ Alternatives to a windowed average, such as an exponentially
weighted moving average (EWMA), were considered, but even an EWMA with
a low alpha was found to still be susceptible to a single, large event
outlier.

+ Testing Bounce with a larger Ball (6x4) but the same size bounding
box for collisions (1x1) results in a slightly smaller bounce-mark,
about 15% less for a standard terminal window (80x24).

+ The threshold was chosen based on many studies that suggested that a
degradation of 10% was at the start of the climb in game loop times as
objects continued to be added.

+ Using bigger objects in Bounce (e.g. a Ball that is 3x3) actually
results in more objects being supported, and not fewer.  The draw()
cost for bigger objects is larger than that of the small objects, but
there are more collisions.  Dragonfly returns the first object found
in a collision, so the more likely a collision, the less likely the
entire set of objects is searched.

+ Using a smaller screen with Bounce has the same effect as using a
larger object (i.e. it can support more objects).

