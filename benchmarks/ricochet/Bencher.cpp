//
// Bencher.cpp
//

#include <iostream>
#include <vector>
#include <algorithm>		// for nth_element
#include <numeric>

using std::vector;

// game includes
#include "Ball.h"
#include "Bencher.h"

// engine includes
#include "Config.h"
#include "EventStep.h"
#include "GameManager.h" 
#include "DisplayManager.h"
#include "LogManager.h"
#include "WorldManager.h"

// Constructor
Bencher::Bencher(char *time, char *number) {
    setSolidness(df::SPECTRAL);
    registerInterest(df::STEP_EVENT);
    clock_gettime(CLOCK_REALTIME, &tmp);
    log = "timer_"+std::to_string(tmp.tv_sec)+"_"+std::string(time)+"_"+std::string(number)+".log" ;
    this->start_time = tmp.tv_sec * NANOCONST + tmp.tv_nsec;
    this->ballnbrs = strtoul(number, NULL, 10);
    this->duration = strtoul(time, NULL, 10) * 60;
    // initalizing Balls up to a number;
    for (int i = 0; i < this->ballnbrs; i++) {
        new Ball;

    }
    df::LogManager &log_manager = df::LogManager::getInstance();
    log_manager.writeMyLog(log,"Step\t time(s)\t time(ns)\t delta(ms)");
    // bench_mark = 0;
    step_count = 0;
}

// Handle event
int Bencher::eventHandler(const df::Event *p_e) {

    df::LogManager &log_manager = df::LogManager::getInstance();

    // Every step...
    if (p_e->getType() == df::STEP_EVENT) {
        clock_gettime(CLOCK_REALTIME, &tmp);
        step_count++;
        ns.ts = tmp;
        ns.c = clock.delta();
        log_manager.writeMyLog(log, "%ld\t %ld\t %ld\t %ld.", step_count, tmp.tv_sec, tmp.tv_nsec, ns.c);
        frames_delta.push_back(ns.c);
        frames_timespec.push_back(ns.ts);

        if (tmp.tv_sec * NANOCONST + tmp.tv_nsec - this->start_time > this->duration * NANOCONST){
            stopBounce();
        }

        return 1;
    }

    // If we get here, we have ignored this event.
    return 0;
}

// Stop this iteration.  If this is the third iteration, stop the game.
void Bencher::stopBounce() {
    static int tally = 0;        // count total
    tally += step_count;
    df::LogManager &log_manager = df::LogManager::getInstance();
    double avg_delta = std::accumulate(frames_delta.begin(), frames_delta.end(), 0.0) / frames_delta.size();
    long st = frames_timespec.begin()->tv_sec * NANOCONST + frames_timespec.begin()->tv_nsec;
    long en = frames_timespec.back().tv_sec * NANOCONST + frames_timespec.back().tv_nsec;
    double avg_time = (en - st) / frames_timespec.size();
    //printf ("Start: %ld , End:  %ld, size :%ld\n", st, en, frames_timespec.size());
    int n = frames_delta.size() / 2;
    nth_element(frames_delta.begin(), frames_delta.begin() + n, frames_delta.end());
    long median = frames_delta[n];
    log_manager.writeLog("AVG DELTA : %lf - AVG NANOSEC TIME: %lf - MEDIAN DELTA %ld", avg_delta, avg_time, median);
    log_manager.writeLog("AVG sec per frame: %lf / AVG FPS: %lf", avg_time/NANOCONST, NANOCONST/avg_time);
    df::GameManager::getInstance().setGameOver();
}


//Return average number of objects supported (bencher-mark).
int Bencher::getBenchMark() {
    return 0;
}
