//
// Ball.h
//

#include "Color.h"
#include "Object.h"

#define BALL_CHAR 'o'

class Ball : public df::Object {
 private:
  enum df::Color color;

 public:
  Ball();
  int eventHandler(const df::Event *p_e);
  void setVel();
  void out();
  void draw();
};


