//
// Bouncer.h
//

#include <stdio.h>

#include "Object.h"
#include "Clock.h"

#define LAST 30
#define ITERATION_MAX 3
#define TARGET_LIMIT  1.1 * df::FRAME_TIME_DEFAULT

class Bouncer : public df::Object {
 private:
  int step_count;		// number of steps
  int q[LAST];			// queue of game loop times
  int head;			// head of queue
  int iteration;		// iteration number
  df::Clock clock;		// keep track of loop timing
  int bounce_mark;              // number of objects supported
  bool do_particles;		// true if particle benchmark
  void particleBurst();		// add particle burst

 public:
  Bouncer(bool do_particles);
  int getBounceMark();	
  void stopBounce();
  int eventHandler(const df::Event *p_e);
};


