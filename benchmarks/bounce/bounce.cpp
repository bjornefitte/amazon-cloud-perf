//
// Bounce - a Dragonfly Benchmark
//
// Copyright: Mark Claypool and WPI, 2012, 2105
//

#include <stdlib.h>

// Engine includes
#include "Config.h"
#include "Fraps.h"
#include "LogManager.h"
#include "GameManager.h"

// Game includes
#include "Bouncer.h"

#define BOUNCE_VERSION 3.0

int main(void){ 
  df::LogManager &log_manager = df::LogManager::getInstance();
  log_manager.setLogLevel(1);

  // Print version info.
  //printf("Bounce - a Dragonfly Benchmark (v%.1f)\n", BOUNCE_VERSION);

  // Start up GameManager.
  df::GameManager &game_manager = df::GameManager::getInstance();
  if (game_manager.startUp())  {
    log_manager.writeLog("Error starting game manager!");
    game_manager.shutDown();
    exit(1);
  }

  // Bouncer creates Balls/Particles and records stats.
  std::string s = df::Config::getInstance().match("particle");
  bool do_particles;
  if (s == "true")
    do_particles = true;
  else
    do_particles = false;
  log_manager.writeLog("BOUNCE: Doing %s test.",
		       do_particles ? "Particle" : "Object");
  Bouncer *p_bouncer = new Bouncer(do_particles);

  // Add fraps.
  new df::Fraps;
  //  new Fraps(false); // enable
  //  new Fraps(true);  // disable

  // Run the game (this blocks until the game loop is over).
  game_manager.run();

  // Get stats.
  int bounce_mark = p_bouncer -> getBounceMark();

  // Shut game engine down.
  game_manager.shutDown();

  // Print statistics.
 // printf("** Average maximum number of %s: %d **\n",
 //	 do_particles ? "particles" : "objects", bounce_mark);
  printf("%d\n", bounce_mark);
  return 0;
}
