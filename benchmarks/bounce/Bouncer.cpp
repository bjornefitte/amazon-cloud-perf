//
// Bouncer.cpp
//

#include <iostream>
#include <vector>
#include <algorithm>		// for nth_element

using std::vector;

// game includes
#include "Ball.h"
#include "Bouncer.h"

// engine includes
#include "Config.h"
#include "EventStep.h"
#include "GameManager.h" 
#include "DisplayManager.h" 
#include "LogManager.h"
#include "WorldManager.h"

// Constructor
Bouncer::Bouncer(bool do_particles) {
  this->do_particles = do_particles;
  setSolidness(df::SPECTRAL);
  registerInterest(df::STEP_EVENT);
  iteration = 0;

  // Set up for initial run by clearing queue.
  for (int i=0; i<LAST; i++)
    q[i] = 0;
  head = 0;

  bounce_mark = 0;
  step_count = 0;
}

// Handle event.
int Bouncer::eventHandler(const df::Event *p_e) {
  df::LogManager &log_manager = df::LogManager::getInstance();

  // Every step...
  if (p_e->getType() == df::STEP_EVENT) {

    step_count++;

    // Create ball/particles.
    if (do_particles) {
      particleBurst();
    } else
      new Ball;

    // Record how long game loop took.
    long int delta = clock.delta();
    log_manager.writeLog(5, "Bouncer::eventHandler(): Delta is %d.", delta);

    // Update queue.
    q[head] = delta;		// Add most recent to queue.
    head = (head + 1) % LAST;   // Circular queue.

    // Compute median.
    vector<int> temp_vector;
    for (int i=0; i < LAST; i++)
      temp_vector.push_back(q[i]);
    int n = temp_vector.size()/2;
    nth_element (temp_vector.begin(), temp_vector.begin()+n,temp_vector.end());
    int median = temp_vector[n]/1000;

    // Write to log.
    log_manager.writeLog("BOUNCE: Step %d - %ld of %d msec ( median %d )",
			 step_count, delta/1000, 
			 df::FRAME_TIME_DEFAULT, median);

    // When over target, stop iteration.
    if (median >= TARGET_LIMIT)
      stopBounce();
    return 1;
  }

  // If we get here, we have ignored this event.
  return 0;
}

// Stop this iteration.  If this is the third iteration, stop the game.
void Bouncer::stopBounce() {
  static int tally = 0;		// count total

  // Update stats for this iteration.
  iteration++;
  tally += step_count;
  df::LogManager &log_manager = df::LogManager::getInstance();
  log_manager.writeLog("BOUNCE: Iteration %d - step count: %d", 
		       iteration, step_count);

  // If this is the final iteration, shut things down.
  if (iteration == ITERATION_MAX) {
    bounce_mark = tally/ITERATION_MAX;
    if (do_particles) {
      std::string s = df::Config::getInstance().match("burst");
      if (s != "")
	bounce_mark *= atoi(s.c_str());
    }
    log_manager.writeLog("BOUNCE: Average max %s: %d", 
			 do_particles ? "particles" : "objects", bounce_mark);
    df::GameManager::getInstance().setGameOver();
    return;
  }

  // Otherwise, set up stats for another run by clearing queue.
  step_count = 0;
  for (int i=0; i<LAST; i++)
    q[i] = 0;
  head = 0;

  // Remove all Balls/Particles.
  df::WorldManager &world_manager = df::WorldManager::getInstance();
  df::ObjectList objs = world_manager.getAllObjects();
  df::ObjectListIterator i(&objs);
  for (i.first(); !i.isDone(); i.next()) {
    df::Object *p_o = i.currentObject();
    if (p_o->getType() == "ball" || p_o->getType() == "Particle") 
      world_manager.markForDelete(p_o);
    // Below used for particle manager.
    // df::ParticleManager::getInstance().clear();
  }

}

// Return average number of objects supported (bounce-mark).
int Bouncer::getBounceMark() {
  return bounce_mark;
}

// Add particle burst.
void Bouncer::particleBurst() {
  df::DisplayManager &graphics_manager = df::DisplayManager::getInstance();
  std::string s;

  // Defaults if no match in df-config.txt.
  int number = 50;
  int number_spread = 0;
  float size = 1.0;
  float size_spread = 0.0;
  float speed = 0.3;
  float speed_spread = 0;
  float age = 100000;  // Make large so particles don't expire.
  int age_spread = 0;
  unsigned char opacity = 255;
  int opacity_spread = 0;
  df::Vector direction(0,0);
  float direction_spread = 0.0;
  float position_spread = 0.0;
  int r = 255;
  int g = 255;
  int b = 255;
  unsigned char color_spread = 0;
  
  // Number of particles.
  s = df::Config::getInstance().match("burst");
  if (s != "")
    number = atoi(s.c_str());
  s = df::Config::getInstance().match("burst-spread");
  if (s != "")
    number_spread = atoi(s.c_str());
  
  // Speed.
  s = df::Config::getInstance().match("speed");
  if (s != "")
    speed = atof(s.c_str());
  s = df::Config::getInstance().match("speed-spread");
  if (s != "")
    speed_spread = atof(s.c_str());
  
  // Size.
  s = df::Config::getInstance().match("size");
  if (s != "")
    size = atof(s.c_str());
  s = df::Config::getInstance().match("size-spread");
  if (s != "")
    size_spread = atof(s.c_str());
  
  // Age.
  s = df::Config::getInstance().match("age");
  if (s != "")
    age = atoi(s.c_str());
  s = df::Config::getInstance().match("age-spread");
  if (s != "")
    age_spread = atoi(s.c_str());
  
  // Opacity.
  s = df::Config::getInstance().match("opacity");
  if (s != "")
    opacity = atoi(s.c_str());
  s = df::Config::getInstance().match("opacity-spread");
  if (s != "")
    opacity_spread = atoi(s.c_str());
  
  // Color.
  s = df::Config::getInstance().match("color-r");
  if (s != "")
    r = atoi(s.c_str());
  s = df::Config::getInstance().match("color-g");
  if (s != "")
    g = atoi(s.c_str());
  s = df::Config::getInstance().match("color-b");
  if (s != "")
    b = atoi(s.c_str());
  s = df::Config::getInstance().match("color-spread");
  if (s != "")
    color_spread = atoi(s.c_str());
  
  // Direction.
  s = df::Config::getInstance().match("direction-x");
  if (s != "")
    direction.setX(atof(s.c_str()));
  s = df::Config::getInstance().match("direction-y");
  if (s != "")
    direction.setY(atof(s.c_str()));
  s = df::Config::getInstance().match("direction-spread");
  if (s != "")
    direction_spread = atof(s.c_str());
  
  // Position spread.
  s = df::Config::getInstance().match("position-spread");
  if (s != "")
    position_spread = atof(s.c_str());
  
  // Location.
  df::Vector cell(graphics_manager.getHorizontal()/2, 
		  graphics_manager.getVertical()/2);

  // Add particles.
  df::addParticles(number, number_spread, 
		   cell, position_spread,
		   direction, direction_spread, 
		   size, size_spread,
		   speed, speed_spread,
		   age, age_spread, 
		   opacity, opacity_spread,
		   r, g, b, color_spread);

  return;
}
