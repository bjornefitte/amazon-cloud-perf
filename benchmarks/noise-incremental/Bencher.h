//
// Bencher.h
//

#include <stdio.h>
#include <time.h>
#include <vector>
#include "Object.h"
#include "Ball.h"
#include "Clock.h"
#include <sstream>
#define NANOCONST 1e9
#define SEC 60
#define LAST 30
#define ITERATION_MAX 3
#define TARGET_LIMIT  1.1 * df::FRAME_TIME_DEFAULT
typedef struct {
    long c;
    struct timespec ts;
} nanosec;

class Bencher : public df::Object {
private:
    std::vector<Ball> ballsvec;
    int step_count;        // number of steps
    // int q[LAST];			// queue of game loop times
    //int head;			// head of queue
    //int iteration;		// iteration number
    df::Clock clock;        // keep track of loop timing
    std::vector<long int> frames_delta; // Vector of Deltas !
    std::vector<struct timespec> frames_timespec;
    std::string log ;
    long start_time;
    unsigned int increment;
    unsigned int Loop_count;
    long duration;
    int ballnbrs;
    struct timespec tmp;
    nanosec ns;
    void Inactivate();
    void Activate(unsigned int);
    unsigned int getRandom (int start, int end);
    //int bench_mark;              // number of objects supported
    //bool do_particles;		// true if particle benchmark
    //void particleBurst();		// add particle burst

public:
    Bencher(char *time, char *number, char* unused);

    int getBenchMark();

    void stopBounce();

    int eventHandler(const df::Event *p_e);
};


