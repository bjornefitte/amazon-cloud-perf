//
// Bencher.cpp
//

#include <iostream>
#include <vector>
#include <algorithm>		// for nth_element
#include <numeric>

using std::vector;

// game includes
//#include "Ball.h"
#include "Bencher.h"

// engine includes
#include "Config.h"
#include "EventStep.h"
#include "GameManager.h" 
#include "DisplayManager.h"
#include "LogManager.h"
#include "WorldManager.h"

// Constructor
Bencher::Bencher(char *time, char *number, char* secondly) {
    setSolidness(df::SPECTRAL);
    registerInterest(df::STEP_EVENT);
    clock_gettime(CLOCK_REALTIME, &tmp);
    log = "timer_"+std::to_string(tmp.tv_sec)+"_"+std::string(time)+"_"+std::string(number)+".log" ;
    this->start_time = tmp.tv_sec * NANOCONST + tmp.tv_nsec;
    this->ballnbrs = strtoul(number, NULL, 10);
    this->duration = strtoul(time, NULL, 10) * 60;
    // initalizing Balls up to a number;
    for (int i = 0; i < this->ballnbrs; i++) {
        Ball b = new Ball(false); // Inactive.
        ballsvec.push_back(b);
    }
  //  df::LogManager &log_manager = df::LogManager::getInstance();
   // log_manager.writeMyLog(log,"Step\t time(s)\t time(ns)\t delta(ms)");
    // bench_mark = 0;
    step_count = 0;
    this->increment = this->ballnbrs * 5 / 100;
    this->Loop_count = 0;
}
unsigned int Bencher::getRandom (int start, int end){
    std::random_device                  rand_dev;
    std::mt19937                        generator(rand_dev());
    std::uniform_int_distribution<unsigned int>  distr(start, end);
    return distr(generator);
}
void Bencher::Inactivate(){
    for (unsigned int i = 0; i < ballsvec.size(); i++){
        ballsvec.at(i).setActive(false);
    }
}
void Bencher::Activate(unsigned int number) {
    if (number > ballsvec.size())
        exit(-1); // This should never happen
    for (unsigned int i = 0 ; i < number;i++) {
        ballsvec.at(i).setActive(true);
    }
}
// Handle event
int Bencher::eventHandler(const df::Event *p_e) {

   // df::LogManager &log_manager = df::LogManager::getInstance();

    // Every step...
    if (p_e->getType() == df::STEP_EVENT) {
        Inactivate();
        Inactivate();
        clock_gettime(CLOCK_REALTIME, &tmp);

        long modulo = this->duration * 5 / 100;
        if ( step_count/30 % (modulo)==0 && step_count % 30 == 0){

            this->Loop_count++;
            Activate(this->increment * this->Loop_count);
        }
        clock_gettime(CLOCK_REALTIME, &tmp);
        step_count++;
        if (tmp.tv_sec * NANOCONST + tmp.tv_nsec - this->start_time > this->duration * NANOCONST){
           // printf("What segfaults?\n");
            stopBounce();
        }

        return 1;
    }

    // If we get here, we have ignored this event.
    return 0;
}

// Stop this iteration.  If this is the third iteration, stop the game.
void Bencher::stopBounce() {
    df::GameManager::getInstance().setGameOver();//XXX: Why does it segfault
}


//Return average number of objects supported (bencher-mark).
int Bencher::getBenchMark() {
    return 0;
}
