//
// Ball.cpp
//

#include <stdlib.h> // for rand()

#include "Ball.h"

// Game engine includes.
#include "EventCollision.h"
#include "EventOut.h"
#include "DisplayManager.h"
#include "LogManager.h"
#include "WorldManager.h"

#define SPEED_FACTOR 10

// Constructor.
Ball::Ball() {

  // Set ball attributes.
  setType("ball");
  setVel();
  color =  static_cast <enum df::Color> (rand()%8);

  // Random starting position.
  df::WorldManager &world_manager = df::WorldManager::getInstance();
  df::Vector temp_pos(rand()%(int)world_manager.getBoundary().getHorizontal(),
		      rand()%(int)world_manager.getBoundary().getVertical());
  setPosition(temp_pos);

  // If want to test particles...
  //setSolidness(df::SPECTRAL);
}
Ball::Ball(bool isActive) {
// Set ball attributes.
    setType("ball");
    setVel();
    color =  static_cast <enum df::Color> (rand()%8);

    // Random starting position.
    df::WorldManager &world_manager = df::WorldManager::getInstance();
    df::Vector temp_pos(rand()%(int)world_manager.getBoundary().getHorizontal(),
                        rand()%(int)world_manager.getBoundary().getVertical());
    setPosition(temp_pos);
    setActive(isActive);
}

// Set random speed and direction.
void Ball::setVel() {
  float x, y;
  if (rand()%2)
    x = -1.0/(rand()%SPEED_FACTOR+1);
  else
    x = 1.0/(rand()%SPEED_FACTOR+1);
  if (rand()%2)
    y = -1.0/(rand()%(SPEED_FACTOR*8)+1);
  else
    y = 1.0/(rand()%(SPEED_FACTOR*8)+1);
  setVelocity(df::Vector(x,y));
}

// Handle event.
int Ball::eventHandler(const df::Event *p_e) {

  if (p_e->getType() == df::COLLISION_EVENT) {
    setVel();
    return 1;
  }

  if (p_e->getType() == df::OUT_EVENT) {
    out();
    return 1;
  }

  // If we get here, we have ignored this event.
  return 0;
}

// Bounce off sides of screen.
void Ball::out(){
  df::WorldManager &world_manager = df::WorldManager::getInstance();

  // Left edge.
  if (getPosition().getX() < 0) {
    setVelocity(df::Vector(getVelocity().getX() * -1, getVelocity().getY()));
    df::Vector p(1, getPosition().getY());
    setPosition(p);
  }

  // Right edge.
  if (getPosition().getX()>world_manager.getBoundary().getHorizontal()-1) {
    setVelocity(df::Vector(getVelocity().getX() * -1, getVelocity().getY()));
    df::Vector p(world_manager.getBoundary().getHorizontal()-2, 
		 getPosition().getY());
    setPosition(p);
  }

  // Top edge.
  if (getPosition().getY() < 0) {
    setVelocity(df::Vector(getVelocity().getX(), getVelocity().getY() * -1));
    df::Vector p(getPosition().getX(), 1);
    setPosition(p);
  }
  
  // Bottom edge.
  if (getPosition().getY()>world_manager.getBoundary().getVertical()-1) {
    setVelocity(df::Vector(getVelocity().getX(), getVelocity().getY() * -1));
    df::Vector p(getPosition().getX(), 
		 world_manager.getBoundary().getVertical()-2);
    setPosition(p);
  }

}

// Draw ball on screen.
void Ball::draw() {
  df::DisplayManager &graphics_manager = df::DisplayManager::getInstance();
  graphics_manager.drawCh(getPosition(), BALL_CHAR, color);
}
