import os
import sys
from optparse import OptionParser
usage = "Usage: automate.py [-h] [ -r <nbr> --random=<nbr>| -i <nbr> --incremental=<nbr>]"
def incremental_noise(argv):
      for i in range (25,125, 25):
         os.system ("noise-incremental/noise-inc 5 "+ str(int(argv) * 2)+" &") #Up to 200% of the noise                                                                         #Run in Background
         os.system("ricochet/ricochet 5 "+str(int (float(i)/100 * argv)))
      os.system("mkdir -p incremental")
      os.system("mv timer* incremental/")

def random_noise(argv):
     for i in range (50,500, 25):
         os.system ("noise-random/noise-rnd 5 "+ str(int(argv) * 2)+" &") #Up to 200% of the noise                                                               #Run in Background
         os.system("ricochet/ricochet 5 "+str(int (float(i)/100 * argv)))
     os.system("mkdir -p random")
     os.system("mv  timer* random/")

if __name__ == "__main__":
    parser = OptionParser(usage=usage)
    parser.add_option("-r", "--random", help="The generated noise is randomly distributed", type="int")
    parser.add_option("-i", "--incremental", help="The generated noise is incremental", type="int")
    (options, args) = parser.parse_args()
    if options == {}:
        parser.error (usage)
    if (options.random and options.incremental):
        parser.error("Options -r and -i are mutually exclusive")

    if (options.random):
         random_noise (int(options.random))
    elif (options.incremental):
        incremental_noise (int(sys.argv[2]))
    else:
        parser.print_help()



