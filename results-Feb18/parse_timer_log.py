import sys
import numpy as np
import matplotlib.pyplot as mpl
unparsed_list = []
frame_numbers = []
deltas = []
fps = []

def open_and_parse(file):
    f = open(file,"r")
    i = 0
    for line in f:
        if i != 0:
            unparsed_list.append(line.strip().split(' '))
        i+=1
    close(f)

def prepare_data():
    for i, element in enumerate(unparsed_list):
        frame_numbers.append(i)
        deltas.append(1e6/float(element[-1]))
def plot_data(machine, objects, minutes):
    ax = mpl.figure()
    fig = ax.add_subplot(111)
    fig.set_title('Frames per second vs Frames \n for %s objects in %s for a duration of %s minutes'%(objects, machine, minutes))
    fig.set_xlabel('Frames count')
    fig.set_ylabel('Frames per Second')
    fig.plot(frame_numbers,deltas)
    mpl.show()

if __name__ == "__main__":
    open_and_parse(sys.argv[1])
    prepare_data()
    minutes = sys.argv[1].split('/')[-1].split('.')[0].split('_')[-2]
    objects = sys.argv[1].split('/')[-1].split('.')[0].split('_')[-1]
    machine = sys.argv[1].split('/')[-2]
    print minutes, objects, machine
    #print frame_numbers
    plot_data(machine, objects,minutes )
